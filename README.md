S3 Header Fixer
===

This was a project I had to crank out quickly for a client, so the code could use some refactoring, but it served its purpsoe quite well and they ended up using it for a while.

The problem the client was facing was that the program they used to import images into their S3 bucket were all being written with a mime type of 'S3object' instead of their appropriate mime.  They were then serving theses images on their main ecommerce site, and some browsers choked on non-image-mime image tags.

This program pulls down the header metadata for every file in a bucket, then pushes fixed header data with the correct mime type according to the file extension.  Being a highly IO bound process, Node.js was a great solution, and this script averaged 3 million rewrites in just under 2 hours.
 
