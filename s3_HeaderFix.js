/*
#!/usr/bin/node

Sean Moore
8-30-2011

Create an Amazon object that issues HTTP REST requests to a bucket.
The bucket returns XML lists of 1000 objects in the bucket.
Process the XML, and for each object, verify it, rewrite the headers, and copy it.
Amazon object uploads the new headers with a PUT copy request.
Request the next 1000 objects, starting with the last object from the previous request.

Dependencies:
knox - s3 API wrapper
mime - mime header analysis
xml2js - converts flat XML to JSON

Flow:
requestPage (key) ->
  parser.end (XML) ->
    copyObject(object)
    rewriteObjectHeaders(object)
    requestPage() //cycle
*/

// Increase the max concurrent requests to one host
var http = require('http');
http.Agent.defaultMaxSockets = 1500;

var minutes = 0;
// Timer output
setInterval(function(){console.log('\n* ' + (minutes++) + 'min *\n');}, 60000);

process.on('uncaughtException', function(err) {
    // Resume requests from last known good write
    requestPage()
});

var s3 = {
    key: 'MY_KEY',
    secret: 'MY_SECRET',
    bucket: 'images.example.com'
}
    
var backBucket = {
    key: 'BACKUP_KEY',
    secret: 'BACKUP_SECRET',
    bucket: 'backup.images.example.com'
}

var count = 0,
    pages = 0,
    first = true;

var knox = require('knox').createClient(s3),
    //back_bucket = require('knox').createClient(backBucket)
    xmljs = require('xml2js'),
    parser = new xmljs.Parser({normalize:false}),
    mime = require('mime'),
    fs = require('fs'),
    url = require('url');

var lastWrite = fs.readFileSync('lastItem', 'utf8');
if (lastWrite.substring(0,1) !== 'z'){
    fs.writeFileSync('errors.txt', '');
}
lastWrite = null;

var fileStream = fs.createWriteStream('errors.txt')


// If something goes haywire, try to resume from last item write.
process.on('uncaughtException', function(err) {
    // Resume requests from last known good write
    var lastWrite = fs.readFileSync('lastItem', 'utf8');
    if (lastWrite.substring(0,1) !== 'z'){
	if ((pages * 1000 - count) > 15000) {console.log('Critical Error, Exiting.'); process.exit(1)}
        console.log('Uncaught exception, resuming.');
        requestPage(lastWrite);
    } else {
        console.log('Finished!');
        process.exit();
    }
});


/*
  Gets the next page of objects in the bucket.  API specifies this caps at
  1000, but it could be lower than that. s3 lists in alphabetical order, always.

  On a result, the XML is passed to the xml2js parser.

  @param string|null key The first object to retrieve
*/
function requestPage(key){
    knox.get('/?marker=' + encodeURIComponent(key)).on('response', function(res){
        var xml = '';
        res.setEncoding('utf8');
        res.on('data', function(data){
            xml += data;
        });
        res.on('end', function(){
            pages++;
            console.log('Stack size: ' + (pages*1000 - count));
            parser.parseString(xml);
            
            // Die if the stack size gets too big, probably a problem here.
            if ((pages*1000 - count) > 15000){
                console.log('Stack size too big, panic, exit.');
                process.exit(2);
            }
        });
        res.on('error', function(err){
            console.log(err);
        });
    }).on('error', function(err){ console.log(err);}).end()
}

/*
  Rewrites object headers for an item already in an s3 bucket.
  uses the x-amz-copy-source direcive to PUT the same key into itself (in-place-copy).
  x-amx-metadeta-directive of REPLACE specifies not to copy existing headers.
  x-amz-meta-uploader is a custom header to see that this script touched an object.

  Has some conditional logging to console.out every 5000 operations

  @param object obj An object describing an s3 object: key, and mime type
*/
function rewriteObjectHeaders(obj){
    var req = knox.put('/' + encodeURIComponent(obj.key), {
        'Content-Type': obj.type,
        'Content-Length': '0',
        'x-amz-copy-source': '/' + s3.bucket + '/' + encodeURIComponent(obj.key),
        'x-amz-metadata-directive': 'REPLACE',
        'x-amz-meta-uploader': 's3Fixer'
    }); 
    req.on('response', function(res){
        res.on('data', function(data){
        });
        res.on('end', function(){
            count++;
            if(count%5000 == 0) console.log('Processed: ' + count.toString() + ' ' + obj.key)
            obj = null;
        });
        res.on('error', function(error){
            console.log(error);
        });
        if (res.statusCode != 200){
            console.log('Error: ' + res.statusCode);
            fileStream.write(res.statusCode + ' ' + url.parse(req.url).pathname + '\n');
        }
    }).on('error', function(err){ console.log(err);});
}

/*
  The same as above rewriteObjectHeaders, except specifies a different bucket to copy to.

  @param object obj JSON describing an s3 object {key, mime type}
*/

function copyObject(obj){
    var req = back_bucket.put('/' + encodeURIComponent(obj.key), {
        'Content-Type': obj.type,
        'Content-Length': '0',
        'x-amz-copy-source': '/' + backBucket.bucket + '/' + obj.key,
        'x-amz-metadata-directive': 'REPLACE',
        'x-amz-meta-uploader': 's3Fixer'
    }); 
    req.on('response', function(res){
        res.on('data', function(data){
        });
        res.on('end', function(){
            obj = null;
        });
        res.on('error', function(err){
            console.log(err);
        });
        if (res.statusCode != 200){
            console.log('Error: ' + res.statusCode + ' ' +req.url);
        }
    }).on('error', function(err){ console.log(err);});
}

parser.addListener('end', function(xmlObj){ 
    // Contents is the main XML container for items
    var items = xmlObj.Contents;
    var ilen = items.length;
    console.log('Returned: ' + ilen.toString());
  
    // We request from the last object processed, inclusive.
    // This removes that redundant object.
    if(first === false){
        items.splice(0,1);
    } else {
        first = false;
    }

    // First item is spliced, so length of array is actually off by one
    for(var i = 0; i < (ilen - 1); i++){
        type = mime.lookup(items[i].Key);
        var s3Headers = {
            key: items[i].Key,
            len: items[i].Size,
            type: type
        }

        rewriteObjectHeaders(s3Headers);
        //copyObject(s3Headers);

    }
    
    // Gets new page of results starting with last item from current page
    var lastKey = items.pop().Key;
    fs.writeFile('lastItem', lastKey)
    requestPage(lastKey);
});


// Process.argv[2] is the first command line parameter.  
// This can be used to start the script from a previously known object in case of failure.
var prefix = (process.argv[2] === undefined) ? '' : process.argv[2];

requestPage(prefix);
